<?php

namespace Drupal\pagedesigner_responsive_images\Plugin\pagedesigner\Handler;

use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner_gallery\Plugin\pagedesigner\Handler\GalleryGallery;
use Drupal\pagedesigner_media\Plugin\MediaFieldHandlerBase;
use Drupal\ui_patterns\Definition\PatternDefinitionField;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process entities of type "gallery_gallery".
 *
 * @PagedesignerHandler(
 *   id = "responsive_image_gallery_gallery",
 *   name = @Translation("gallery handler"),
 *   types = {
 *      "gallery_gallery",
 *   },
 * )
 */
class ResponsiveImageGalleryGallery extends GalleryGallery {

  /**
   * File url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator = NULL;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setFileUrlGenerator($container->get('file_url_generator'));
    return $instance;
  }

  /**
   * Set the file url generator.
   *
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file url generator.
   */
  public function setFileUrlGenerator(FileUrlGeneratorInterface $file_url_generator) {
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public function collectAttachments(array &$attachments) {
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(PatternDefinitionField &$field, array &$fieldArray) {
  }

  /**
   * {@inheritdoc}
   */
  public function get(Element $entity, string &$result = '') {
    $data = $this->elementHandler->serialize($entity);
    $stringData = [];
    foreach ($data as $item) {
      $stringData[] = $item['alt'];
    }
    return implode(' ', $stringData);
  }

  /**
   * {@inheritdoc}
   */
  public function getContent(Element $entity, array &$list = [], $published = TRUE) {
  }

  /**
   * {@inheritdoc}
   */
  public function serialize(Element $entity, array &$result = []) {
    $data = [];
    if ($entity->children) {
      foreach ($entity->children as $item) {
        if ($item->entity != NULL) {
          $medium = MediaFieldHandlerBase::getMediaTranslation($item->entity->field_media->entity);
          if ($medium != NULL && $medium->field_media_image->entity != NULL) {
            $file = $medium->field_media_image->entity;
            $url = $previewUrl = $file->createFileUrl(FALSE);
            /** @var \Drupal\image\Entity\ImageStyle $style */
            $style = $this->entityTypeManager
              ->getStorage('image_style')
              ->load('pagedesigner_default');
            if ($style != NULL) {
              $url = $previewUrl = $this->fileUrlGenerator->transformRelative($style->buildUrl($file->getFileUri()));
            }
            /** @var \Drupal\image\Entity\ImageStyle $style */
            $style = $this->entityTypeManager
              ->getStorage('image_style')
              ->load('thumbnail');
            if ($style != NULL) {
              $previewUrl = $this->fileUrlGenerator->transformRelative($style->buildUrl($file->getFileUri()));
            }

            $data[] = [
              'alt' => $item->entity->field_content->value,
              'id' => $item->entity->field_media->target_id,
              'src' => $url,
              'preview' => $previewUrl,
              'uri' => $file->getFileUri(),
            ];
          }
        }
      }
    }
    $result = $data + $result;
  }

  /**
   * {@inheritdoc}
   */
  public function describe(Element $entity, array &$result = []) {
  }

  /**
   * {@inheritdoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function patch(Element $entity, array $data) {
  }

  /**
   * {@inheritdoc}
   */
  public function copy(Element $entity, Element $container = NULL, Element &$clone = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function delete(Element $entity, bool $remove = FALSE) {
  }

  /**
   * {@inheritdoc}
   */
  public function restore(Element $entity) {
  }

  /**
   * {@inheritdoc}
   */
  public function view(Element $entity, string $view_mode, array &$build = []) {
  }

  /**
   * {@inheritdoc}
   */
  public function publish(Element $entity) {
  }

  /**
   * {@inheritdoc}
   */
  public function unpublish(Element $entity) {
  }

}
