<?php

namespace Drupal\pagedesigner_responsive_images\Plugin\pagedesigner\Handler;

use Drupal\Component\Serialization\Yaml as YamlSerializer;
use Drupal\pagedesigner\Plugin\HandlerConfigTrait;
use Drupal\pagedesigner\Plugin\pagedesigner\Handler\Longtext;
use Symfony\Component\Yaml\Yaml as YamlParser;

/**
 * Process entities of type "component_sizes".
 *
 * @PagedesignerHandler(
 *   id = "component_sizes",
 *   name = @Translation("Image handler"),
 *   types = {
 *     "component_sizes",
 *   },
 * )
 */
class ComponentSizes extends Longtext {

  // Import config property and setter, auto set in HandlerConfigTrait::create.
  use HandlerConfigTrait;

  /**
   * {@inheritdoc}
   */
  public function collectAttachments(array &$attachments) {
    $attachments['library'][] = 'pagedesigner_responsive_images/pagedesigner';
    $config = $this->configFactory->get('pagedesigner_responsive_images.settings');
    $attachments['drupalSettings']['pagedesigner_responsive_images']['sizes'] = YamlParser::parse(YamlSerializer::decode($config->get('grid_sizes')));

  }

}
