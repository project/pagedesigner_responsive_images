<?php

namespace Drupal\pagedesigner_responsive_images\Form;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Add the pagedesigner effects settings form.
 */
class PagedesignerResponsiveImagesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pagedesigner_responsive_images.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pagedesigner_responsive_images_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    parent::buildForm($form, $form_state);
    $config = $this->config('pagedesigner_responsive_images.settings');

    $form['grid_sizes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Set grid sizes'),
      '#default_value' => Yaml::decode($config->get('grid_sizes')),
    ];

    // Add checkbox to enable background image resizing.
    $form['enable_responsive_background_images'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable resizing of background images (requires manual cache rebuild)'),
      '#default_value' => $config->get('enable_responsive_background_images'),
    ];

    // Add checkbox to enable inheriting background images.
    $form['inherit_responsive_background_images'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Inherit larger background images to lower resolutions with adapated image style (requires manual cache rebuild)'),
      '#default_value' => $config->get('inherit_responsive_background_images'),
      '#states' => [
        'enabled' => [
          ':input[name="enable_responsive_background_images"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      Yaml::encode($form_state->getValue('grid_sizes'));
    }
    catch (InvalidDataTypeException) {
      $form_state->setErrorByName(
        'grid_sizes',
        $this->t('The provided configuration is not a valid yaml text.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('pagedesigner_responsive_images.settings');
    $config->set('grid_sizes', Yaml::encode($form_state->getValue('grid_sizes')));
    $config->set('enable_responsive_background_images', $form_state->getValue('enable_responsive_background_images'));
    $config->set('inherit_responsive_background_images', $form_state->getValue('inherit_responsive_background_images'));
    $config->save();
  }

}
