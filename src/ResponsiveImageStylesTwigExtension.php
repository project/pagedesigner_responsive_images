<?php

namespace Drupal\pagedesigner_responsive_images;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Serialization\Yaml as YamlSerializer;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml as YamlParser;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Extend Drupal's Twig_Extension class.
 */
class ResponsiveImageStylesTwigExtension extends AbstractExtension {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file URL generator service.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $urlGenerator;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Creates an Responsive Image Styles Twig Extension.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileUrlGenerator $urlGenerator
   *   The file URL generator service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logging service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileUrlGenerator $urlGenerator, LoggerChannelFactoryInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->urlGenerator = $urlGenerator;
    $this->logger = $logger->get('pagedesigner_responsive_images');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('file_url_generator'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('image_style', $this->getImageStyles(...)),
      new TwigFunction('styled_image_url', $this->getStyledImageUrl(...)),
    ];
  }

  /**
   * Returns associative array from $string.
   *
   * @param array|object $sizes
   *   JSON encoded component sizes. Can be of mulitple types.
   * @param string $template
   *   Image style template.
   *
   * @return array
   *   Array containing the image styles.
   */
  public function getImageStyles($sizes, string $template) {

    $imageStyles = [];
    try {
      if (\is_array($sizes) && !empty($sizes['#text'])) {
        $sizes = Json::decode((string) $sizes['#text']);
      }

      if (\is_object($sizes) && $sizes::class == Markup::class) {
        $sizes = Json::decode($sizes->jsonSerialize());
      }

      if (\is_string($sizes) && !empty($sizes)) {
        $sizes = Json::decode($sizes);
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage(), $e->getTrace());
      return $imageStyles;
    }

    $config = $this->entityTypeManager->getStorage('image_style_template')->load($template);
    if ($config == NULL) {
      $config = $this->entityTypeManager->getStorage('image_style_template')->load('image_standard');
    }

    $settings = YamlParser::parse(YamlSerializer::decode($config->settings));
    foreach ($settings as $breakpoint => $templates) {
      $imageStyles[$breakpoint] = [
        'templates' => $templates,
        'size'      => $sizes[$breakpoint] ?? '',
      ];
    }
    return $imageStyles;
  }

  /**
   * Return URL of a styled image.
   *
   * @param string $uri
   *   Image Uri.
   * @param string $styleName
   *   Image style.
   *
   * @return string
   *   URL to image after image style is applied.
   */
  public function getStyledImageUrl(string $uri, string $styleName = '') {
    $default_url = $this->urlGenerator->transformRelative(
      $this->urlGenerator->generateString($uri)
    );
    if (empty($styleName)) {
      return $default_url;
    }
    /** @var \Drupal\image\Entity\ImageStyle $style */
    $style = $this->entityTypeManager->getStorage('image_style')->load($styleName);
    if (empty($style)) {
      return $default_url;
    }
    return $this->urlGenerator->transformRelative($style->buildUrl($uri));
  }

}
