<?php

namespace Drupal\pagedesigner_responsive_images\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Service to manage background image resizing.
 */
class BackgroundImageResize {

  /**
   * The pagedesigner responsive config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected static $config = NULL;

  /**
   * The extracted background images.
   *
   * @var array
   */
  protected static $extractedBackgroundImages = [];

  /**
   * The extracted background images.
   *
   * @var array
   */
  protected static $styleMappings = [
    'large' => 'pdri__2000',
    'medium' => 'pdri__1000',
    'small' => 'pdri__800',
  ];

  /**
   * Create a new instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    static::$config = $config_factory->get('pagedesigner_responsive_images.settings');
  }

  /**
   * Alter the background image url to match the appropiate viewport size.
   *
   * If there's no image set for the given size but a larger one,
   * a background image property with the appropiate size will be added.
   *
   * @param string $css
   *   The css for the element.
   * @param string $size
   *   The size to process it for.
   * @param int $id
   *   The element to process for.
   */
  public function processCss(string &$css, string $size, int $id) {
    if (static::$config->get('enable_responsive_background_images')) {
      $this->extract($css, $size, $id);
      $this->apply($css, $size, $id);
    }
  }

  /**
   * Extract any background image set on the css.
   *
   * @param string $css
   *   The css.
   * @param string $size
   *   The size the css is applied to.
   * @param int $id
   *   The id of the element.
   */
  public function extract(string $css, string $size, int $id) {
    $matches = [];
    if (preg_match('/background-image:url\((.*?)\)/', $css, $matches) != FALSE) {
      count($matches);
      if (empty(static::$extractedBackgroundImages[$id])) {
        static::$extractedBackgroundImages[$id] = [];
      }
      static::$extractedBackgroundImages[$id][$size] = $matches[1];
    }
  }

  /**
   * Apply the css alteration.
   *
   * If there's no image set for the given size but a larger one,
   * a background image property with the appropiate size will be added.
   *
   * @param string $css
   *   The css.
   * @param string $size
   *   The size the css is applied to.
   * @param int $id
   *   The id of the element.
   */
  public function apply(string &$css, string $size, int $id) {
    if (!empty(static::$extractedBackgroundImages[$id])) {
      if (!empty(static::$extractedBackgroundImages[$id][$size])) {
        $css = $this->alterPath($css, static::$styleMappings[$size]);
      }
      elseif (static::$config->get('inherit_responsive_background_images')) {
        switch ($size) {
          case 'small':
            if (!empty(static::$extractedBackgroundImages[$id]['medium'])) {
              $path = $this->alterPath(static::$extractedBackgroundImages[$id]['medium'], static::$styleMappings['small']);
            }
            elseif (!empty(static::$extractedBackgroundImages[$id]['large'])) {
              $path = $this->alterPath(static::$extractedBackgroundImages[$id]['large'], static::$styleMappings['small']);
            }
            break;

          case 'medium':
            if (!empty(static::$extractedBackgroundImages[$id]['large'])) {
              $path = $this->alterPath(static::$extractedBackgroundImages[$id]['large'], static::$styleMappings['medium']);
            }
        }
        if (!empty($path)) {
          $css .= ';background-image:url(' . $path . ')';
        }
      }
    }
  }

  /**
   * Alter a path to include a style.
   *
   * @param string $string
   *   The string to alter.
   * @param string $style
   *   The style to use.
   */
  public function alterPath(string $string, string $style) {
    return preg_replace('#sites/default/files/([\w]{2})#', 'sites/default/files/styles/' . $style . '/public/$1', $string);
  }

}
