
# Pagedesigner responsive images

Module that adds support for responsive images to pagedesigner components.
It provides all the information needed to create a `<picture>` element
that contains multiple images for different viewport sizes, as well as
multiple resolutions of each image.

There's a submodule *pagedesigner_focal_point* that enables focal point
based cropping for images


## Concept / How does it work?

At the end of the day, a rendered responsive image should look something
like this:

    <picture>
      <source media="(min-width: 1200px)" sizes="350px" srcset="/path/to/x-large.img 2000w,/path/to/large.img 1000w">
      <source media="(min-width: 768px)"  sizes="50vw"  srcset="/path/to/medium.img 600w">
      <source media="(min-width: 300px)"  sizes="100vw" srcset="/path/to/medium.img 100w">
      <img src="/path/to/fallback.img" loading="lazy" >
    </picture>

There are 3 critical attributes inside the `<sources>` tag:
- **scrsct**
The source set defines the URLs to different images and their sizes.
Drupal's image styles are used to generate all all the images and
get their URLs. For the handling of image styles, this module
introduces *image style templates* ([more on image style templates](#image-style-templates)) which are collections of image
styles. An image style template maps different viewport sizes
to different sets of image styles.

- **sizes**
The width of the image when it's rendered. Inside pagedesigner, the
compontent's will be calculated based on it's parents and their
dimensions ([more on component size calculation](#component-size-calculation)). Since this is strongly depending on the theme implementation,
it is possible to configure of rules that map component withs to CSS
selectors per viewport.

- **media**
A media condition that need to be fullfilled for the source to be showed.


The module provides 2 new pagedesigner fields for responsive image handling:
- **ImageStyleTemplate**
Provides a selection field to choose from the defined image style templates.

- **ComponentSizes**
Automatically calculates the size of the component based on its parents.

The **ResponsiveImageHandler** – an extension to the standard image handler
– then returns the image's URI which is used to apply the image styles.

## Usage
### Add responsive images to a custom pattern.
Add image style template and component fields to pattern definition.

    pattern_id:
      fields:
        field_name_image:
          type: image
          label: The image
        field_name_image_template:
          type: image_style_template
          label: Template
        field_name_sizes:
          type: component_sizes
          label: Size

Add responsive image definition to pattern:

    responsive_images:
      template_fields:                               // Mapping of image fields to template fields
        field_name_image: field_name_image_template  // Image field > image template field
      component_sizes_field: field_name_sizes        // Name of sizes field

Use the following twig code for the image.

    {% include '@pagedesigner_responsive_images/includes/responsive-image.html.twig' with {
      'image' : field_name_image,                    // Field name of image field
      'template' : field_name_image_template,        // Field name of image style template field
      'sizes': field_name_sizes,                     // Field name of component sizes field
        'attributes' : {
          ... additional attributes                  // Additional attributes for the <img> tag, e.g. title, alt etc.
        }                                            // Per default, loading is set to lazy - can be overriden.
     } %}


### Add responsive images to a custom template
Use the following twig code for the image.

    {% include '@pagedesigner_responsive_images/includes/responsive-image.html.twig' with {
      'image' : image.entity.field_media_image.entity.uri.value,     // URI of the image
      'template' : 'image_standard',                                 // Machine name of image style template
      'sizes': {'1200px':'350px','768px':'50vw','300px':'100vw'},    // The size of the rendered image, per breakpoint.
      'attributes' : {
        ... additional attributes                                    // Additional attributes for the <img> tag, e.g. title, alt etc.
      }                                                              // Per default, loading is set to lazy - can be overriden.
     } %}

### Advanced usage without including the provided template
Run! Or use this Twig code instead of the one above:

    {#
      image: URI to image
      template: Machine name of image style template
      sizes: Associative array containing the sizes
    #}

    {% if sizes is not empty and template|render is not empty %}
      <picture>
        {% for key, value in image_style(sizes, template) %}
          <source
            media="(min-width: {{ key }})"
            sizes="{{ value.size }}"
            srcset="{% for image_style_name, size in value.templates %}{{ styled_image_url(image|render, image_style_name) }} {{ size }} {% if not loop.last %},{% endif %}{% endfor %}" >
          </source>
        {% endfor %}
        <img src="{{file_url(image)}}" ... additional attributes />
      </picture>
    {% endif %}


## Advanced

### Image style templates

Image style templates are definitions that map viewport widths to drupal image styles and define the resulting image size after applying the style. The definition works with YAML syntax:

    viewport width:
      image style machine name: image width after style is applied


example:

    1200px:
      pdri_1920_400__2000: 2000w
      pdri_1920_400__1000: 1000w
      pdri_1920_400__500: 500w
      pdri_1920_400__200: 200w
      pdri_1920_400__100: 100w
    768px:
      pdri_1920_400__1000: 1000w
      pdri_1920_400__500: 500w
      pdri_1920_400__200: 200w
      pdri_1920_400__100: 100w
    300px:
      pdri_3_1__500: 500w
      pdri_3_1__200: 200w
      pdri_3_1__100: 100w


Image style templates can be managed at the following link:
*/admin/config/pagedesigner/responsive-images/image-style-templates*


### Component size calculation
Define the rules at the following link:
*/admin/config/pagedesigner/responsive-images*

Use YAML syntax as follows:

    .row.fullwidth:
      1200px: 100vw
      768px: 100vw
      300px: 100vw
    .row:
      1200px: min( 100vw, 1140px )
      768px: 100vw
      300px: 100vw
    .col-md-1:
      1200px: 0.8333333
      768px: 0.8333333
      300px: 1
    .col-md-2:
      1200px: 0.16666667
      768px: 0.16666667
      300px: 1

**The calculation then works as follows:**
Starting from a given component, we recursively go through all parent components. For each parent component, we check if there is a matching rule and choose the first rule that applies. If the rules value is numeric, it will be multiplied with the parents size. Otherwise the the mininium of both values is calculated.

Example:

    <div class="row">
      <div class="col-md-1">
        <img id="component-1"/>
      </div>
    </div>

    <div class="row fullwidth">
      <div class="col-md-1">
        <img id="component-2"/>
      </div>
    </div>

The sizes for `#component-1` would then be:\
`calc( 1 * 100vw )`, for screensizes >= 300px\
`calc( 0.8333333 * 100vw )`, for screensizes >= 768px\
`calc( 0.8333333 * min( 100vw, 1140px ) )` for screensizes > 1200px

The sizes for `#component-2` would then be:\
`calc( 1 * 100vw )`, for screensizes >= 300px\
`calc( 0.8333333 * 100vw )`, for screensizes >= 768px\
`calc( 0.8333333 * 100vw )` for screensizes >= 1200px\
