<?php

namespace Drupal\pagedesigner_responsive_images\PathProcessor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Alter image style paths to accept slashes in params.
 */
class ImageStylePathProcessor implements InboundPathProcessorInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file URL generator service.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $urlGenerator;

  /**
   * Creates an Image Style Path Processor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileUrlGenerator $urlGenerator
   *   The file URL generator service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileUrlGenerator $urlGenerator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->urlGenerator = $urlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'), $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {

    if (str_starts_with($path, '/sites/default/files/styles/')) {

      if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $request->getPathInfo())) {
        $pathParts = explode('/', str_replace('/sites/default/files/styles/', '', $request->getPathInfo()));
        $imageStyle = $pathParts[0];
        array_shift($pathParts);
        $uriBase = array_shift($pathParts);
        $imgUri = $uriBase . '://' . rawurldecode(implode('/', $pathParts));

        $style = $this->entityTypeManager->getStorage('image_style')->load($imageStyle);

        if ($style) {
          $imgUrl = $this->urlGenerator->transformRelative($style->buildUrl($imgUri));
          $response = new RedirectResponse($imgUrl);
          $response->send();
        }
      }
    }
    return $path;
  }

}
