<?php

/**
 * @file
 * The module file.
 */

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\node\Entity\Node;

/**
 * Implements hook_pagedesigner_css_statement_alter().
 */
function pagedesigner_responsive_images_pagedesigner_css_statement_alter(string &$css, array $context) {
  \Drupal::service('pagedesigner_responsive_images.background_image_resizer')->processCss($css, $context['key'], $context['id']);
}

/**
 * Register module to theme namespaces to allow inclusion of twig templates.
 *
 * @param array $build
 *   The render array.
 * @param \Drupal\node\Entity\Node $node
 *   The current entity.
 * @param \Drupal\Core\Entity\EntityViewDisplay $display
 *   The current entity view display.
 * @param string $view_mode
 *   The view mode.
 */
function pagedesigner_responsive_images_node_view(array &$build, Node $node, EntityViewDisplay $display, $view_mode) {
  if ($view_mode != 'full') {
    return;
  }
  $pagedesignerService = \Drupal::service('pagedesigner.service');

  if ($pagedesignerService->isEditMode()) {
    $build['#attached']['drupalSettings']['pagedesigner']['theme_namespaces']['@pagedesigner_responsive_images'] = '/' . \Drupal::service('extension.list.module')->getPath('pagedesigner_responsive_images') . '/templates';
  }
}
